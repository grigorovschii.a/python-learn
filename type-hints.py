#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def average(a: int, b: int, c: int) -> float:
    return (a + b + c) / 3


print(round(average(1, 7, 15), 1))
print(average(1, 15, 10))
