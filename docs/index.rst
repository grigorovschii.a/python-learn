.. deom documentation master file, created by
   sphinx-quickstart on Fri Apr 19 18:38:26 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to demo's documentation!
================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

This is a subtitle
------------------

`My Webpage <https://alxg.co.uk>`_

This script demonstrates how to filter directories based on a specific file extension.
The os module is imported to interact with the operating system::

   import os

The ends_with function takes two arguments: dirs (a directory path) and search (a file extension).
It lists all directories in the given path and checks if each directory ends with the specified file extension.
If a match is found, the directory name is printed::

   def ends_with(dirs, search):
       for dir in os.listdir(dirs):
           if dir.endswith(search):
               print(dir)

This is a subtitle 2
====================

The following code is executed when the script is run directly, not imported::

   if __name__ == "__main__":
       The ends_with function is called with the current directory (".") and the ".py" file extension.
       ends_with(".", ".py")


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
