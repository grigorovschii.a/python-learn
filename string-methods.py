#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This script demonstrates how to filter directories based on a specific file extension.
The os module is imported to interact with the operating system.
"""
import os

"""
The ends_with function takes two arguments: dirs (a directory path) and search (a file extension).
It lists all directories in the given path and checks if each directory ends with the specified file extension.
If a match is found, the directory name is printed.
"""


def ends_with(dirs: str, search: str):
    for dir in os.listdir(dirs):
        if dir.endswith(search):
            print(dir)


"""
The following code is executed when the script is run directly, not imported.
"""
if __name__ == "__main__":
    """
    The ends_with function is called with the current directory (".") and the ".py" file extension.
    """
    ends_with(".", ".py")
