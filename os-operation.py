#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import subprocess
import datetime
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-n", "--name", help="Name of the user", type=str)
parser.add_argument("--year", help="DOB year", type=int)
args = parser.parse_args()

current_dir = os.getcwd()
list_dir = os.listdir(current_dir)

for list in list_dir:
    print(list)

date_object = datetime.date.today()
date_object = date_object.year

age = date_object - args.year
bio = f"Hi {args.name}, you are {age} years old."
print(bio)

numlist = "1,2,3,4,5,6,7,8,9,10"
print(numlist.split(","))
print(numlist.split(",", 3))

firstlastname_list = ["John", "Doe"]
firstlastname_str = "John Doe"
print("_".join(firstlastname_list))
print("_".join(firstlastname_str))

try:
    with open("someinputs.txt") as file:
        if "drew" in file.read():
            print("Found it!")
        else:
            print("Not found!")
except FileNotFoundError as e:
    print("File not found!")
