#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Get details of loan

money_owed = float(input("How much money do you owe in £: "))  # £30,000
apr = float(input("Annual percentage rate: "))  # 3%
pay = float(input("How much will you pay off each month: "))  # £1,000
months = int(input("How many months do you want to see the results for: "))  # 24

monthly_rate = apr / 100 / 12
print(f"Monthly rate is: {monthly_rate}")

for x in range(months + 1):
    # Calculate interest to pay
    interest_paid = money_owed * monthly_rate
    print(f"Apart from 30,000, you pay: {interest_paid}")

    # Add in interest
    money_owed = money_owed + interest_paid
    print(f"Now you owe: {money_owed}")

    if money_owed - pay < 0:
        print(f"The last payment is {money_owed}")
        print(f"You paid off the load in {x / 12} years")
        break

    # Make payment
    money_owed = money_owed - pay

    print(f"Paid {pay}, of which {interest_paid} was interest")
    print(f"Now I owe: {money_owed}")
    print(x)
