#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def find_acr():
    user_input = input("What acronym do you want?\n")

    found = False
    try:
        with open("software_acronyms.conf") as acronyms:
            for acronym in acronyms:
                if user_input in acronym:
                    print(acronym)
                    found = True
                    break
    except FileNotFoundError as e:
        print("File not found")
        return

    if not found:
        print("Acronym does not exist.\n")


def add_acr():
    user_input_acr = input("What acronym do you want to add?\n")
    user_input_def = input("What is the definition of your acronym?\n")

    with open("acronyms.conf", "a") as file:
        file.write(f"{user_input_acr.upper()} - {user_input_def}")


def main():
    choice = input("Do you want to find(F) or Add(A) an acronym? ")
    if choice.upper() == "F":
        find_acr()
    elif choice.upper() == "A":
        add_acr()
    else:
        print("Please use (F) for find or (A) for add\nHappy pythoning :) ")


main()
