#!/usr/bin/env python3
# -*- coding: utf-8 -*-

user_input_acr = input("What acronym do you want to add?\n")
user_input_def = input("What is the definition of your acronym?\n")

with open("acronyms.conf", "a") as file:
    file.write(f"{user_input_acr} - {user_input_def}")
