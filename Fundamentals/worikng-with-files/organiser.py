#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import shutil

# Path to the Desktop directory
desktop_directory = os.path.expanduser("~/Desktop")

# Dictionary containing the Directory names and their corresponding file extension
directories = {
    "Images": [".jpeg", ".png", ".jpg", ".gif"],
    "Documents": [".doc", ".docx", ".pdf", ".txt"],
    "Archives": [".zip", ".tar", ".rar"],
}

# Create the subdirectories if they don't exist.
for directory in directories:
    directory_path = os.path.join(desktop_directory, directory)
    if not os.path.exists(directory_path):
        os.makedirs(directory_path)

# Move files to the corresponding subdirectory
for file_name in os.listdir(desktop_directory):
    original_file_path = os.path.join(desktop_directory, file_name)
    if os.path.isfile(original_file_path):
        for directory_name, extensions in directories.items():
            for extension in extensions:
                if file_name.endswith(extension):
                    destination_directory = os.path.join(
                        desktop_directory, directory_name
                    )
                    shutil.move(original_file_path, destination_directory)
