#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os

dir_items = ["FILES", "DIRECTORIES"]
main_dir = "/home/alexg/Desktop"

try:
    for i in dir_items:
        os.mkdir(f"{main_dir}/{i}")
        print(f"Directory {i.title()} Created")
except FileExistsError as e:
    print(f"Directory {i.title()} already exist")


for entry in os.scandir(main_dir):

    src_location = os.path.join(main_dir, entry.name)
    files_dest_location = os.path.join(f"{main_dir}/FILES", entry.name)

    if os.path.isfile(src_location):
        os.rename(src_location, files_dest_location)
