#!/usr/bin/env python3
# -*- coding: utf-8 -*-

age = int(input("Your age\n"))

decades = age // 10
years = age % 10

print(f"You are {decades} decades and {years} year(s) old")
