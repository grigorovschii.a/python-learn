#!/usr/bin/env python3
# -*- coding: utf-8 -*-

temp = 30
weather = "sunny"
sunny = True

if temp > 25 and weather != "sunny":
    print("Stay inside")
else:
    print("Go for a walk")

if sunny:
    print("Cool, go swimming")
else:
    print("It's not sunny today")
