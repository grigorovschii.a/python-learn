#!/usr/bin/env python3
# -*- coding: utf-8 -*-

computer = "scissors"
user = input(f"Choose rock, paper or scissors:\n-> ")

if computer == user:
    print("No one won!")
elif user == "rock" and computer == "scissors":
    print("You WON")
elif user == "paper" and computer == "rock":
    print("You WON")
elif user == "scissors" and computer == "paper":
    print("You WON")
else:
    print("You lost, ma friend. HA!")
