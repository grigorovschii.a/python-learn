#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random

roll = random.randint(1, 6)
guess = int(input("Guess the dice roll:\n"))

if roll == guess:
    print(f"I rolled {roll}. Hey, you guessed it")
else:
    print(f"I rolled {roll}. Hey, try one more time ;)")
