#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random

computer = random.choice(["rock", "scissors", "paper"])
user = input(f"Choose rock, paper or scissors:\n-> ")

print(f"Computer choice is: {computer}")

if computer == user:
    print("No one won!")
elif user == "rock" and computer == "scissors":
    print("You WON")
elif user == "paper" and computer == "rock":
    print("You WON")
elif user == "scissors" and computer == "paper":
    print("You WON")
else:
    print("You lost, ma friend. HA!")
