#!/usr/bin/env python3

expenses = [10.50, 8, 5, 15, 20, 5, 3]

money = 0

for x in expenses:
    money = money + x

total = sum(expenses)

print(f"You spent, £{total}")
print(f"You spent, £{money}")

expenses = []
total = 0

num_expenses = int(input("Enter num of expenses: "))

for n in range(num_expenses):
    expenses.append(float(input("Take my money ;) -> ")))

total = sum(expenses)

print(f"You spent, £{total}")
