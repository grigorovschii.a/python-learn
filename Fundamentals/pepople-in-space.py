#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests
import json

response = requests.get("http://api.open-notify.org/astros.json")
response_fmt = response.json()

print("Name of the people ins space are:")

for people in response_fmt["people"]:
    print(people["name"])
    # print(f'People name: {people["name"]} and the craft is {people["craft"]}')

print(f'Total number of people in space are: {response_fmt["number"]}')
