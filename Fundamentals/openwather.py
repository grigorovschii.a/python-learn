#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests
import os

api = os.getenv("WEATHER_API")
location = input("Type desired location: ")
weather_url = (
    f"https://api.weatherapi.com/v1/current.json?key={api}&q={location}&aqi=no"
)

chuck_url = "https://api.chucknorris.io/jokes/random"

get_chuck_joke = requests.get(url=chuck_url)
chuck_response = get_chuck_joke.json()

print(f'{chuck_response["value"]}\n')

response = requests.get(
    url=weather_url,
)
weather_json = response.json()

temp = weather_json["current"]["temp_c"]
condition = weather_json["current"]["condition"]["text"]

# print(weather_json)

print(f"Today in {location} temperature is: {temp} and condition is {condition}")
