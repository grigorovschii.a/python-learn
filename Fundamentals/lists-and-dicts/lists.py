#!/usr/bin/env python3
# -*- coding: utf-8 -*-

abvs = ["LOL", "IDK", "SMH", "TBH"]
word = "TBH"

abvs.append("WFH")
abvs.append("IMHO")
abvs.remove("LOL")

if word in abvs:
    print(f"{word} is in the list.")
else:
    print(f"{word} is not in the list")

for abv in abvs:
    print(abv)

print(abvs)
