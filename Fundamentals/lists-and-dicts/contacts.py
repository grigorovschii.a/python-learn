#!/usr/bin/env python3
# -*- coding: utf-8 -*-

contacts = {
    "number": 3,
    "students": [
        {"name": "AlexG", "email": "AlexG@gmail.com"},
        {"name": "DoinaG", "email": "DoinaG@gmail.com"},
        {"name": "AgnesG", "email": "AgnesG@gmail.com"},
    ],
}

print("Students Emails:\n")

for student in contacts["students"]:
    print(f'Student name is: {student["name"]}, Email: {student["email"]}')
