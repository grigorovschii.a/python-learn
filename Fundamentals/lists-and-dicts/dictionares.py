#!/usr/bin/env python3
# -*- coding: utf-8 -*-

acronyms = {"LOL": "Laugh out loud", "IDK": "I don't know", "TBH": "To be honest"}

acronyms["TBH"] = "Honestly"

del acronyms["LOL"]
tbh = acronyms.get("LOL")

if tbh:
    print(tbh)
else:
    print(f"Key doesn't exist.")

create_sentence = acronyms.get("IDK") + " what happened " + acronyms.get("TBH")

print(create_sentence)
print(f"Sentence v2: {acronyms.get('IDK')} what happened {acronyms.get('TBH')}")
