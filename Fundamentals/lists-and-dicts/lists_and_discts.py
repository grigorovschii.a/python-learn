#!/usr/bin/env python3.11
# -*- coding: utf-8 -*-

menus = {
    "Breakfast": ["Egg", "Bagel", "Coffee"],
    "Lunch": ["Beans", "Rice", "TACO"],
    "Dinner": ["Salmon", "Greek salad", "Tartuffe"],
}

# For k - key and v - value in menu.items, print.
for k, v in menus.items():
    print(f"\nFor {k} we have: {', '.join(v)}")
