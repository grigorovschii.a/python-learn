#!/usr/bin/env python3
# -*- coding: utf-8 -*-

current_movies = {}

current_movies["Grinch"] = "11:00"
current_movies["Snowman"] = "13:00"
current_movies["Terminator"] = "20:00"
current_movies["Spider-Man"] = "23:00"

print("We're showing he following movies:\n")
for k in current_movies:
    print(k)

movie = input("\nWhat movie'd like the showtime for?\n")
showtime = current_movies.get(movie)

if not showtime:
    print(f"\nYour {movie.upper()} doesn't exist")
else:
    print(f"\nHEY, the {movie} showtime is {showtime}")
