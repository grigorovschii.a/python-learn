#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class Employee:
    def __init__(self, fname, lname, salary) -> None:
        self.fname = fname
        self.lname = lname
        self.salary = salary

    def calculate_paycheck(self):
        return self.salary / 12
