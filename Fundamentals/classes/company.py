#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from employee import Employee


class Company:
    def __init__(self) -> None:
        self.employees = []

    def add_employee(self, new_employee):
        self.employees.append(new_employee)

    def display_employees(self):
        print("List of Employees:")
        print(f"----------------------------------")
        for e in self.employees:
            print(e.fname, e.lname)

    def pay_employees(self):
        print("\nPaying Employees:")
        print(f"----------------------------------")
        for e in self.employees:
            print(f"Paycheck for: {e.fname}, {e.lname}")
            print(f"Amount: £{int(e.calculate_paycheck()):,}")


def main():
    my_company = Company()

    Toronto = Employee("Bob", "Toronto", 50000)
    my_company.add_employee(Toronto)
    Chicago = Employee("Bob", "Chicago", 60000)
    my_company.add_employee(Chicago)
    LasVegas = Employee("Bob", "LasVegas", 70000)
    my_company.add_employee(LasVegas)

    my_company.display_employees()
    my_company.pay_employees()


main()
