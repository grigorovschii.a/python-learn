#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class Vacuum_Robot:
    def __init__(self, name, type):
        self.name = name
        self.type = type

    def clean(self):
        print("Cleaning now...")


robot = Vacuum_Robot("Nik", "Vacuum")
print(robot.name)
print(robot.type)

robot.clean()
