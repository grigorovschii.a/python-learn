#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random

player1 = "C++"
player2 = "JS"


def dice_roll():
    dice_total = random.randint(1, 6) + random.randint(1, 6)
    return dice_total


def main():
    roll1 = dice_roll()
    roll2 = dice_roll()

    print(f"{player1} rolled {roll1}")
    print(f"{player2} rolled {roll2}")

    if roll1 > roll2:
        print(f"{player1} wins!")
    elif roll2 > roll1:
        print(f"{player2} wins!")
    else:
        print("TIE!")


main()
