#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def addition(a, b):
    return a + b


def main():
    num1 = 10
    num2 = 20

    # Calling our function
    result = addition(num1, num2)
    print(f"The result is: {result}")


main()
