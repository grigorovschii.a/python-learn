#!/usr/bin/env bash
# -*- coding: utf-8 -*-

# Define arrays for environments and Xymon servers
qa=("pools_qa_h1-lb01-mgt" "pools_qa_h3-lb01-mgt")
prod=("pools_prod_h1-lb01-mgt" "pools_prod_h2-lb01-mgt" "pools_prod_h3-lb01-mgt")
xymon=("h1-oxym01" "h2-oxym01" "h3-oxym01")

# Get the TLA from the command line argument and convert it to uppercase
tla_uppercase=$(echo "@option.tla@" | tr '[:lower:]' '[:upper:]')

# Display the information about dropping monitors
echo "Dropping monitors for TLA: $tla_uppercase"

# Iterate over the environments and Xymon servers to drop monitors
for env in "${@option.env@[@]}"; do
    for server in "${xymon[@]}"; do
      echo "Run drop monitor for: ${server} ${env} @option.tla@"
      /usr/bin/bb "${server}":1984 "drop ${env} @option.tla@"
    done
done
