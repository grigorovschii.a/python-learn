#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class Employee:
    def __init__(self, name, age, position, salary) -> None:
        self.name = name
        self.age = age
        self.position = position
        self.salary = salary

    def increase_salary(self, percent):
        self.salary += self.salary * (percent / 100)

    def info(self):
        print(
            f"{self.name} is {self.age} years old. Employee is a {self.position} with the salary of £{self.salary}"
        )


employee1 = Employee("Alex", 31, "SRE", 1200)
employee2 = Employee("VIctor", 42, "Developer", 1300)

employee1.increase_salary(20)
employee1.info()
