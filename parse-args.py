#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import datetime
import logging

# Create the parser
parser = argparse.ArgumentParser(
    description="Input your Name and DOB and I'll print your age for you",
    epilog="GET SOME HELP",
)

# Add argument
parser.add_argument("--name", type=str, required=True, help="Give it a name")
parser.add_argument("--year", type=int, help="Type your year")

# Parse the argument
args = parser.parse_args()

today = datetime.date.today()
year = today.year

your_age = year - args.year

print(f"Hi, {args.name}. Your age is {your_age or 2000}")

logging.warning("This is a warning")
logging.info("This is a non critical info")
